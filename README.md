# 005_006_asyncio_treading_multiprocessing_GIL

# Поєднання двох лекцій:
# Асінхронне програмування у Python
# Багатопоточне програмування у Python

[video_1](https://youtu.be/x8dXK4mFk-Y)
[video_2](https://youtu.be/U-xN6J72DPE)

---

### программа навчання: **Python Advanced 2022**

### номер заняття: 5, 6

### засоби навчання: Python; інтегроване середовище розробки (PyCharm або Microsoft Visual Studio + Python Tools for Visual Studio + можливість використання Юпітер Jupyter Notebook)

---

### Огляд, мета та призначення уроку



**Вивчивши матеріал даного заняття, учень зможе:**


**Зміст уроку:**
